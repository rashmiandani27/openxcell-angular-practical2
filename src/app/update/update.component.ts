import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
})
export class UpdateComponent implements OnInit {
  homeForm: FormGroup = new FormGroup({});
  isLoading = false;
  isUpdate = false;
  state: any;

  tempUser: any = localStorage.getItem('cu');
  user: any = JSON.parse(this.tempUser);

  constructor(private route: Router, private crudService: CrudService) {
    this.state = route.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit(): void {
    this.homeForm = new FormGroup({
      id: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      desc: new FormControl('', [Validators.required]),
      author: new FormControl({ value: '', disabled: true }, [
        Validators.required,
      ]),
    });
    if (this.state?.rowdata) this.homeForm.setValue(this.state.rowdata);
    this.isUpdate = this.state.isUpdate;
    if (this.isUpdate) {
      this.id?.disable();
    }
  }

  get id() {
    return this.homeForm.get('id');
  }
  get title() {
    return this.homeForm.get('title');
  }
  get desc() {
    return this.homeForm.get('desc');
  }
  get author() {
    return this.homeForm.get('author');
  }

  onCreate() {
    this.isLoading = true;
    let data = {
      id: this.id?.value,
      title: this.title?.value,
      desc: this.desc?.value,
      author: this.user.username,
    };
    if (this.isUpdate) {
      this.crudService.update(data.id, data).subscribe((data) => {
        this.isLoading = false;
        this.route.navigateByUrl('home');
      });
    } else {
      this.crudService.create(data).subscribe((data) => {
        this.isLoading = false;
        this.route.navigateByUrl('home');
      });
    }
  }
}
