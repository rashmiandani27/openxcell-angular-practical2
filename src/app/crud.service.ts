import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API_URL, LOGIN_URL } from './contant';
import { Event } from './event.model';
import { User } from './user.moder';

@Injectable({
  providedIn: 'root',
})
export class CrudService {
  constructor(private http: HttpClient) {}

  // Register
  register(data: User): Observable<any> {
    return this.http.post(LOGIN_URL, data).pipe(catchError(this.handleError));
  }
  login() {
    return this.http.get<User[]>(LOGIN_URL);
  }

  // Create
  create(data: Event): Observable<any> {
    return this.http.post(API_URL, data).pipe(catchError(this.handleError));
  }

  // Read
  list() {
    return this.http.get<Event[]>(API_URL);
  }

  // Update
  update(id: any, data: Event): Observable<any> {
    return this.http
      .put(API_URL + '/' + id, data)
      .pipe(catchError(this.handleError));
  }

  // Delete
  delete(id: Event): Observable<any> {
    return this.http
      .delete(API_URL + '/' + id)
      .pipe(catchError(this.handleError));
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError('Something bad happened; please try again later.');
  }
}
