import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  regiForm: FormGroup = new FormGroup({});
  isLoading = false;
  constructor(private crudService: CrudService, private route: Router) {}

  ngOnInit(): void {
    this.regiForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$'),
      ]),
      cpassword: new FormControl('', [
        Validators.required,
        Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$'),
      ]),
    });
  }
  get username() {
    return this.regiForm.get('username');
  }
  get email() {
    return this.regiForm.get('email');
  }
  get password() {
    return this.regiForm.get('password');
  }
  get cpassword() {
    return this.regiForm.get('cpassword');
  }

  onRegister() {
    let data = {
      id: Math.random(),
      username: this.username?.value,
      email: this.email?.value,
      password: this.password?.value,
    };
    this.isLoading = true;
    this.crudService.register(data).subscribe((data) => {
      this.isLoading = false;
      this.route.navigateByUrl('login');
    });
  }
}
