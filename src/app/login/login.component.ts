import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({});

  isLoading = false;

  constructor(private crudService: CrudService, private route: Router) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$'),
      ]),
    });
  }

  get email() {
    return this.loginForm.get('email');
  }
  get password() {
    return this.loginForm.get('password');
  }

  onLogin() {
    this.isLoading = true;
    this.crudService.login().subscribe((loginData) => {
      let validUser = false;
      loginData.forEach((data) => {
        this.isLoading = false;
        if (
          data.email === this.email?.value &&
          data.password === this.password?.value
        ) {
          validUser = true;
          this.route.navigateByUrl('home');
          localStorage.setItem('cu', JSON.stringify(data));
        }
      });
      if (!validUser) {
        console.log('Invalid Username and Password');
      }
    });
  }
}
