import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from '../crud.service';
import { Event } from '../event.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  searchText = '';
  events: Event[] = [];

  isLoading = false;
  isUpdate = false;

  tempUser: any = localStorage.getItem('cu');
  user: any = JSON.parse(this.tempUser);

  constructor(private route: Router, private crudService: CrudService) {}

  ngOnInit(): void {
    this.onLoad();
  }

  onLoad() {
    this.crudService.list().subscribe((data) => {
      this.events = data;
    });
  }

  onUpdate(rowdata: Event) {
    if (this.user) {
      this.isUpdate = true;
      this.route.navigate(['update'], {
        state: { rowdata: rowdata, isUpdate: this.isUpdate },
      });
    } else {
      this.route.navigateByUrl('login');
    }
  }
  onCreate() {
    if (this.user) {
      this.isUpdate = false;
      this.route.navigate(['update'], {
        state: { rowdata: null, isUpdate: this.isUpdate },
      });
    } else {
      this.route.navigateByUrl('login');
    }
  }
  onDelete(id: any) {
    if (this.user) {
      this.isLoading = true;

      this.crudService.delete(id).subscribe((data) => {
        this.isLoading = false;
        this.onLoad();
      });
    } else {
      this.route.navigateByUrl('login');
    }
  }
}
