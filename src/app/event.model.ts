export interface Event {
  id: number;
  title: string;
  desc: string;
  author: string;
}
